package web.hotels.webpro.services;

import web.hotels.webpro.entity.Log;
import java.util.List;


public interface MyService {

	Log addLog(Log log);

	List<Log> getLogs();

	void deleteLog(Log log);
	
}
