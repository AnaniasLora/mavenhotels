package web.hotels.webpro.services.impl;

import web.hotels.webpro.entity.*;
import web.hotels.webpro.services.MyService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;


@Service("myService")
@Scope(value = "singleton", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class MyServiceImpl implements MyService {

	@Autowired
	LogDao lDao;
	@Autowired
	HotelDao hDao;

	public Log addLog(Log log) {
		return lDao.save(log);
	}

	public List<Log> getLogs() {
		return lDao.queryAll();
	}

	public void deleteLog(Log log) {
		lDao.delete(log);
	}
	
	public Hotel addHotel(Hotel hotel)
	{
		return hDao.save(hotel);
	}
	public List<Hotel> getHotels() {
		return hDao.queryAll();
	}

	public void deleteLog(Hotel hotel) {
		hDao.delete(hotel);
	}

}
